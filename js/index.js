// first goal: display a list with location names using Knockout.js (add the map, the ajax requests to the third party API later)
// Locations
var map;
var infoWindow;
// Foursquare API
var clientID = 'P4XIR0NSKJMNTSAV0IPGM1YC1QQ1ZOPTOXJHG4YW3TU1QFV1';
var clientSecret = 'ZW34A2NGAZDKYW3PHTCTWOBQFNQICOOSJBEFXE5Y3AJXTAKN';

var locationMarkers = [
    {
        name: 'Harts Pub',
        address : 'Essex St & Gloucester Street, The Rocks NSW 2000, Australia',
        // location: {lat:-33.861526, lng: 151.206435}
        lat:-33.861526,
        lng: 151.206435
    },
    {
        name: 'The Australian Heritage Hotel',
        address : '100 Cumberland St, The Rocks NSW 2000, Australia',
        // location: {lat:-33.859375, lng: 151.207047}
        lat:-33.859375,
        lng: 151.207047
    },
    {
        name: 'The Glenmore Hotel',
        address : '96 Cumberland St, The Rocks NSW 2000, Australia',
        // location: {lat:-33.858399, lng: 151.207416}
        lat:-33.858399,
        lng: 151.207416
    },
    {
        name: 'The Lord Nelson Brewery Hotel',
        address : '19 Kent St, The Rocks NSW 2000, Australia',
        // location: {lat:-33.857821, lng: 151.203349}
        lat:-33.857821,
        lng: 151.203349
    },
    {
        name: 'The Hero of Waterloo',
        address : '81 Lower Fort Street, The Rocks, NSW 2000, Australia',
        // location: {lat:-33.857453, lng: 151.205859}
        lat:-33.857453,
        lng: 151.205859
    },
    {
        name: 'Mercantile Hotel',
        address : '25 George St, The Rocks NSW 2000, Australia',
        // location: {lat:-33.856820, lng: 151.208340}
        lat:-33.856820,
        lng: 151.208340
    },
    {
        name: 'Fortune of War',
        address : '137 George St, The Rocks NSW 2000, Australia',
        // location: {lat:-33.860142, lng: 151.208470}
        lat:-33.860142,
        lng: 151.208470
    },
    {
        name: 'The Argyle',
        address : '18 Argyle St, The Rocks NSW 2000, Australia',
        // location: {lat:-33.858727, lng: 151.207774}
        lat:-33.858727,
        lng: 151.207774
    },
    {
        name: 'Harbour View Hotel',
        address : '18 Lower Fort St, Dawes Point NSW 2000, Australia',
        // location: {lat:-33.856431, lng: 151.207358}
        lat:-33.856431,
        lng: 151.207358
    },
    {
        name: 'Opera Bar',
        address : '18 Lower Fort St, Dawes Point NSW 2000, Australia',
        // location: {-33.857151, 151.214681}
        lat:-33.857151,
        lng: 151.214681
    }

];

// Viewmodel
var ViewModel = function() {
  // http://knockoutjs.com/documentation/computedObservables.html#managing-this
  var self = this;

  this.markersArray = ko.observableArray([]);

  // Push marker to array of markers
  // http://learn.knockoutjs.com/#/?tutorial=collections
  locationMarkers.forEach( function(markerItem) {
    self.markersArray.push( new MapMarker(markerItem) );
  });

  this.query = ko.observable('');

  this.filteredMarkers = ko.computed(function () {
    var filter = self.query().toLowerCase();

    if (!filter) {
      ko.utils.arrayForEach(self.markersArray(), function (item) {
        if (item.marker) {
          item.marker.setVisible(true);
        }
      });
      return self.markersArray();
    } else {
      return ko.utils.arrayFilter(self.markersArray(), function(item) {
        var result = (item.name().toLowerCase().search(filter) >= 0)
        if (item.marker) {
          item.marker.setVisible(result); // this is a new line
        }
        return result;
        })
    }
  });
};

// Model
var MapMarker = function(markerItem) {
  var self = this;

  this.name = ko.observable(markerItem.name);
  this.address = ko.observable('');
  this.lat = ko.observable(markerItem.lat);
  this.lng = ko.observable(markerItem.lng);
  this.phoneNumber = ko.observable('');
  this.url = ko.observable('');

  var fourSquareSearchURL = 'https://api.foursquare.com/v2/venues/search'
  var latLong = '?ll='

  var fourSquareAPIURL = fourSquareSearchURL + latLong + self.lat() +
  ',' + self.lng() + '&client_id=' + clientID + '&client_secret=' + clientSecret +
  '&v=20180101' + '&query=' + self.name();

  $.getJSON(fourSquareAPIURL, function(data) {
    // console.log(data.response.venues[0])
    var result = data.response.venues[0];

    self.address(result.location.formattedAddress);
    self.url(result.url);
    self.phoneNumber(result.contact.formattedPhone);

    var marker = new google.maps.Marker({
      position: {
      lat: self.lat(),
      lng: self.lng()
      },
      animation: google.maps.Animation.DROP,
      map: map,
      formatted_address: self.address(),
      title: self.name(),
      phone_number: self.phoneNumber(),
      url: self.url()
    });
    marker.addListener('click', onMarkerClicked);

    self.marker = marker;

  }).fail(function() {
    alert("Could not retrieve data....");
  });
};

function onMarkerClicked() {
  animateMarker( this );
  displayInfoWindow( this );
};

function drop() {
  for (var i =0; i < markersArray.length; i++) {
    setTimeout(function() {
      addMarkerMethod();
    }, i * 200);
  }
};

function animateMarker( marker ) {
  if (marker.getAnimation() !== null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
    setTimeout(function() {
      marker.setAnimation(null);
    });
    google.maps.event.trigger(self.marker, 'click');
  }
};

function displayInfoWindow( marker ) {
  var self = this;
  var contentString = '<div id="content"></div>' +
  '<h3 id="firstHeading" class="firstHeading">' + marker.title + '</h3>'+
  '<div>Address: ' + marker.formatted_address + '</div>' +
  '<div>' + marker.phone_number + '</div>' +
  '<div>' + marker.url + '</div>' +
  '<div>' + '<img src="Powered-by-Foursquare-one-color-300.png" />' + '</div>';

  if (infoWindow) {
    infoWindow.close();
  }

  infoWindow = new google.maps.InfoWindow({
    content: contentString
  });
  //Info window biznass
  infoWindow.open(map, marker);
};

// Google Maps Init
function initMap() {
  var mapStart = {lat:-33.858727, lng: 151.207774};
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 16,
    center: mapStart
  });


  ko.applyBindings( new ViewModel() );
};

function googleMapsError() {
    alert("Google Maps has failed to load.");
}